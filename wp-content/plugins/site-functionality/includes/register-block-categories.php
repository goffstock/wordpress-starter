<?php
/**
 * Add a block category for Custom Theme if it doesn't exist already.
 *
 * @param array $categories Array of block categories.
 *
 * @return array
 */
function sf_block_categories( $categories ) {
    $category_slugs = wp_list_pluck( $categories, 'slug' );
    return in_array( 'site-functionality', $category_slugs, true ) ? $categories : array_merge(
        $categories,
        array(
            array(
                'slug'  => 'site-functionality',
                'title' => __( 'Custom Blocks', 'site-functionality' ),
                'icon'  => null,
            ),
        )
    );
}
add_filter( 'block_categories', 'sf_block_categories' );