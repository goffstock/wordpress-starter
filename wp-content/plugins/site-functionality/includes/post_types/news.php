<?php
// Replace Posts label as News in Admin Panel 
function sf_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    echo '';
}

function sf_change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'News';
		$labels->singular_name = 'News';
		$labels->menu_name = 'News';
		$labels->name_admin_bar = 'News';

}
add_action( 'init', 'sf_change_post_object_label' );
add_action( 'admin_menu', 'sf_change_post_menu_label' );
