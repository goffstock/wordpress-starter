<?php 
// Register Block
function sf_register_block() {
	acf_register_block_type(array(
		'name'              => 'clients',
		'title'             => __('Block'),
		'description'       => __('Sample Block'),
		'render_template'   => SF_PLUGIN_PATH . 'template-parts/blocks/block.php',
		'enqueue_style'     => SF_PLUGIN_URL . 'css/block.min.css',
		'category'          => 'site-functionality',
		'icon'              => 'screenoptions',
		'keywords'          => array( 'blocks' ),
		'mode' 				=> 'auto',
	));
}