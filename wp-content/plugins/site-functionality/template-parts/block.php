<?php
/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'block-' . $block['id'];

if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "class_name" and "align" values.
$wrap_class = 'row';
$class = 'block constrained';

if( !empty($block['class_name']) ) {
    $class .= ' ' . $block['class_name'];
}

if( !empty($block['align']) ) {
    $class .= ' align' . $block['align'];
}

$section_title = get_field('title');
?>

<div class="<?php echo esc_attr($wrap_class); ?>">
	<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($class); ?>">
		<?php if ( $section_title ) : ?>
			<header class="block__content">
				<h2><?php echo esc_html__($section_title, 'site-functionality'); ?></h2>
			</header>
		<?php endif; ?>
		<div class="block__content">

		</div>
	</section>
</div>


