<?php

/*
Plugin Name: Site Functionality
Plugin URI: http://www.mortstock.com/
Description: Essentials for functionality of the website
Author: Geoff Mortstock
Author URI: http://www.mortstock.com/
*/

if ( !function_exists('do_action') ) {
	exit();
}

// Get plugin Path directory
if ( !defined( 'SF_PLUGIN_PATH' ) ) {
    define( 'SF_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
}

if ( !defined( 'SF_PLUGIN_URL' ) ) {
    define( 'SF_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

// Utilities
// require_once( SF_PLUGIN_PATH . 'includes/utils.php' );

// Post Type Files
// require_once( SF_PLUGIN_PATH . 'includes/post-types/news.php' );

// Custom Gutenberg Blocks
// if( function_exists('acf_register_block_type') ) {
// 	require_once( SF_PLUGIN_PATH . 'includes/register-block-categories.php' );
// 	require_once( SF_PLUGIN_PATH . 'includes/register-blocks.php' );

// 	require_once( SF_PLUGIN_PATH . 'includes/blocks/block.php' );
// }