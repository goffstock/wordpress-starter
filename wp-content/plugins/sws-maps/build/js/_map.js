/*---------------------------------------------
 * Google Map Styles
 *-------------------------------------------*/

function loadMap() {
	// API KEY: AIzaSyAokhOTN-906rCzGSEzKcH5eo6ncyETxBw
	
	if ( document.getElementById('map') ) {
		// Map variables
		var url = WP_URL.templateUrl,
			buildingOverlayImgUrl = url + '/images/overlay-building.png',
			markerImgUrl = url + '/images/overlay-logo.png',
			mapContainer = jQuery('#map'),
			initialLoc = {
				lat: parseFloat(mapContainer.attr('data-start-lat')),
				lng: parseFloat(mapContainer.attr('data-start-long')),
			},
			logoLoc = {
				lat: 47.606628,
				lng: -122.335359,
			},
			initialZoom = parseInt(mapContainer.attr('data-zoom')),
			markerBreakZoom = 16,
			mapStyle = getWrightMapStyle();

		// Base Map
		var map = new google.maps.Map(
			document.getElementById('map'), {
				zoom: initialZoom,
				center: initialLoc,
				styles: mapStyle,
				minZoom: 3, 
				maxZoom: 20,
				scrollwheel: false,
				streetViewControl: false,
				mapTypeControl: false,
				fullscreenControl: false,
			}
		);

		// Building Overlay boundaries
		var buildingImageBounds = {
			north: 47.607744,
			south: 47.606628,
			east: -122.335359,
			west: -122.337032
		};
		// Define overlay
		var buildingOverlay = new google.maps.GroundOverlay(
			buildingOverlayImgUrl,
			buildingImageBounds
		);
		// Add the overlay to the map
		//buildingOverlay.setMap(map);

		// Define Marker Icon
		var markerImg = {
			url: markerImgUrl, 
			scaledSize: new google.maps.Size(100, 58),
		};
		// Define Building Marker
		var logoMarker = new google.maps.Marker({
			position: logoLoc,
			title: '',
			icon: markerImg
		});
		
		// Remove overlay & display marker on load & zoom
		map.addListener('tilesloaded', function(){
			var zoomLevel = map.getZoom();
			if ( zoomLevel >= markerBreakZoom) {
				buildingOverlay.setMap(map);
				logoMarker.setMap(null);
			} else {
				buildingOverlay.setMap(null);
				logoMarker.setMap(map);
			}
		});
        map.addListener('zoom_changed', function() {
			var zoomLevel = map.getZoom();
			if ( zoomLevel >= markerBreakZoom) {
				buildingOverlay.setMap(map);
				logoMarker.setMap(null);
			} else {
				buildingOverlay.setMap(null);
				logoMarker.setMap(map);
			}
		});

		// Add Property Markers
		addPropertMarkers(map);
	}
}

function addPropertMarkers(map) {
	document.querySelectorAll('.map__marker').forEach(item => {
		if ( !item.getAttribute('data-lat') || !item.getAttribute('data-long') ) {
			return;
		}
		// Marker Data
		var latLong = {
				lat: parseFloat(item.getAttribute('data-lat')),
				lng: parseFloat(item.getAttribute('data-long')),
			},
			title = item.getAttribute('data-title'),
			href = item.getAttribute('data-href'),
			address = item.getAttribute('data-address'),
			infoContent = '';
		// Info Window Content
		infoContent += 	'<div class="map__info">';
		infoContent += 		'<h3>' + title + '</h3>';
		if ( href ) {
			infoContent += 	'<a href="' + href + '">View Project</a>';
		}
		if ( address ) {
			infoContent += 	'<a href="https://www.google.com/maps/place/' + address + '" target="_blank">Map</a>';
		}
		infoContent += 	'</div>';

		// Define Info Window
		var infoWindow = new google.maps.InfoWindow({
			content: infoContent,
			maxWidth: null,
		});

		// Define Marker Icon
		var propertyMarkerImg = {
			path: google.maps.SymbolPath.CIRCLE,
			scale: 6,
			fillColor: '#b49b57',
			fillOpacity: 1,
			strokeWeight: 0.4,
		};
		// Define Building Marker
		var propertyMarker = new google.maps.Marker({
			position: latLong,
			title: title,
			icon: propertyMarkerImg
		});

		propertyMarker.setMap(map);

		propertyMarker.addListener('click', function() {
			infoWindow.open(map, propertyMarker);
		});
	});

}

function getWrightMapStyle() {
	return [
		{
			"featureType": "administrative",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "administrative",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#444444"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi.sports_complex",
			"elementType": "labels",
			"stylers": [
				{
					"color": "#707479"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 45
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#ffffff"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#666666"
				},
				{
					"weight": "1"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road.highway.controlled_access",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.highway.controlled_access",
			"elementType": "labels.text",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road.highway.controlled_access",
			"elementType": "labels.icon",
			"stylers": [
				{
					"weight": "1"
				},
				{
					"lightness": "0"
				},
				{
					"gamma": "0.33"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit.line",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit.station",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
				{
					"color": "#d0d2d2"
				},
				{
					"visibility": "on"
				}
			]
		}
	]	
}