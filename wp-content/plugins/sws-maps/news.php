<?php
// Replace Posts label as Articles in Admin Panel 
function wright_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    echo '';
}

function wright_change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'News';
		$labels->singular_name = 'News';
		$labels->menu_name = 'News';
		$labels->name_admin_bar = 'News';

}
add_action( 'init', 'wright_change_post_object_label' );
add_action( 'admin_menu', 'wright_change_post_menu_label' );
