<?php
/*
 * Register Development History Post Type
 **/

function sf_history_post() {

	$labels = array(
		'name'                  => _x( 'History', 'Post Type General Name', 'site-functionality' ),
		'singular_name'         => _x( 'History', 'Post Type Singular Name', 'site-functionality' ),
		'menu_name'             => __( 'History', 'site-functionality' ),
		'name_admin_bar'        => __( 'History', 'site-functionality' ),
		'archives'              => __( 'Item Archives', 'site-functionality' ),
		'attributes'            => __( 'Item Attributes', 'site-functionality' ),
		'parent_item_colon'     => __( 'Parent Item:', 'site-functionality' ),
		'all_items'             => __( 'All Items', 'site-functionality' ),
		'add_new_item'          => __( 'Add New Item', 'site-functionality' ),
		'add_new'               => __( 'Add New', 'site-functionality' ),
		'new_item'              => __( 'New Item', 'site-functionality' ),
		'edit_item'             => __( 'Edit Item', 'site-functionality' ),
		'update_item'           => __( 'Update Item', 'site-functionality' ),
		'view_item'             => __( 'View Item', 'site-functionality' ),
		'view_items'            => __( 'View Items', 'site-functionality' ),
		'search_items'          => __( 'Search Item', 'site-functionality' ),
		'not_found'             => __( 'Not found', 'site-functionality' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'site-functionality' ),
		'featured_image'        => __( 'Featured Image', 'site-functionality' ),
		'set_featured_image'    => __( 'Set featured image', 'site-functionality' ),
		'remove_featured_image' => __( 'Remove featured image', 'site-functionality' ),
		'use_featured_image'    => __( 'Use as featured image', 'site-functionality' ),
		'insert_into_item'      => __( 'Insert into item', 'site-functionality' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'site-functionality' ),
		'items_list'            => __( 'Items list', 'site-functionality' ),
		'items_list_navigation' => __( 'Items list navigation', 'site-functionality' ),
		'filter_items_list'     => __( 'Filter items list', 'site-functionality' ),
	);
	$args = array(
		'label'                 => __( 'History', 'site-functionality' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'page-attributes', ),
		'taxonomies'            => array( 'decade' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-clock',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'rewrite'   => array( 
			'slug' => false,
			'with_front' => false 
		),
		'show_in_rest' => true
	);
	register_post_type( 'history', $args );
}
add_action( 'init', 'sf_history_post', 0 );


function sf_decade() {
	$labels = array(
		'name'                       => _x( 'Decade', 'Taxonomy General Name', 'site-functionality' ),
		'singular_name'              => _x( 'Decades', 'Taxonomy Singular Name', 'site-functionality' ),
		'menu_name'                  => __( 'Decades', 'site-functionality' ),
		'all_items'                  => __( 'All Items', 'site-functionality' ),
		'parent_item'                => __( 'Parent Item', 'site-functionality' ),
		'parent_item_colon'          => __( 'Parent Item:', 'site-functionality' ),
		'new_item_name'              => __( 'New Item Name', 'site-functionality' ),
		'add_new_item'               => __( 'Add New Item', 'site-functionality' ),
		'edit_item'                  => __( 'Edit Item', 'site-functionality' ),
		'update_item'                => __( 'Update Item', 'site-functionality' ),
		'view_item'                  => __( 'View Item', 'site-functionality' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'site-functionality' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'site-functionality' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'site-functionality' ),
		'popular_items'              => __( 'Popular Items', 'site-functionality' ),
		'search_items'               => __( 'Search Items', 'site-functionality' ),
		'not_found'                  => __( 'Not Found', 'site-functionality' ),
		'no_terms'                   => __( 'No items', 'site-functionality' ),
		'items_list'                 => __( 'Items list', 'site-functionality' ),
		'items_list_navigation'      => __( 'Items list navigation', 'site-functionality' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest' 				 => true
	);
	register_taxonomy( 'decade', array( 'history' ), $args );
}
add_action( 'init', 'sf_decade', 0 );
