<?php
/*--------------------------------------------------------------------------
 * Add new dashboard widget
 *------------------------------------------------------------------------ */
  // Create new widgets
  function gm925_dashboard_widget() {
    echo '<p>Welcome to Custom Blog Theme! Need help? Contact the developer <a href="mailto:yourusername@gmail.com">here</a>. For WordPress Tutorials visit: <a href="http://www.wpbeginner.com" target="_blank">WPBeginner</a></p>';
  }

  // Register all widgets
  function gm925_register_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Theme Support', 'gm925_dashboard_widget');
  }

  add_action('wp_dashboard_setup', 'gm925_register_dashboard_widgets');
`