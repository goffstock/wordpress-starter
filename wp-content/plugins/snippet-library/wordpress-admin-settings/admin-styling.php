<?php

/*--------------------------------------------------------------------------
 * Replace Admin Footer
 *------------------------------------------------------------------------ */
  function gm925_admin_footer() {
    echo 'Designed and built by <a href="http://www.mortstock.com" target="_blank">Geoff Mortstock</a></p>';
  }
  add_filter('admin_footer_text', 'gm925_admin_footer');


/*--------------------------------------------------------------------------
 * Remove Dashboard Welcome Panel
 *------------------------------------------------------------------------ */
  remove_action('welcome_panel', 'wp_welcome_panel');

/*--------------------------------------------------------------------------
 * Replace Dashboard Logo
 *------------------------------------------------------------------------ */
  function gm925_custom_logo() {
    $logoLink = '/assets/img/logos/custom-logo.png';
    echo
    '<style type="text/css">
      #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
        background-image: url(' . get_bloginfo('stylesheet_directory') . '$logoLink) !important;
        background-position: 0 0;
        color: rgba(0, 0, 0, 0);
      }
      #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
        background-position: 0 0;
      }
    </style>';
  }
  add_action('wp_before_admin_bar_render', 'gm925_custom_logo');



/*--------------------------------------------------------------------------
 * Remove space at top of page when logged in
 *------------------------------------------------------------------------ */
  function gm925_filter_head() {
    remove_action('wp_head', '_admin_bar_bump_cb');
  }
  add_action('get_header', 'gm925_filter_head');
