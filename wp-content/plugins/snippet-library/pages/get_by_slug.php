<?php 

/*--------------------------------------------------------------------------
 * Get page object by slug
 *-------------------------------------------------------------------------*/
function gm925_get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'page' ) {
  global $wpdb;
  $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_slug, $post_type ) );
  if ( $page )
    return get_page($page, $output);
  return null;
}
