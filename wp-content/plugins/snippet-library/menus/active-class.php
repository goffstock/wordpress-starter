<?php
/*--------------------------------------------------------------------------
 * Add active class to selected menu item
 *------------------------------------------------------------------------ */
function gm925_nav_class($classes, $item){
	if( in_array('current-menu-item', $classes) ){
		$classes[] = 'active ';
	}
	return $classes;
}
add_filter('nav_menu_css_class' , 'gm925_nav_class' , 10 , 2);
