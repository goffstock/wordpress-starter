<?php
  function callRestApi($url, $arg1, $arg2) {
    $url = $url . $arg1 . $arg2;
    $header = array(
      'Content-Type: application/json',
      //'Authorization: Authentication xxxxx'
    )
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($curl);
    $result = json_decode($response, true);

    return $result;

    curl_close($curl);
  }
?>
