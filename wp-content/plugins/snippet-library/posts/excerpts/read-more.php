<?php
/*--------------------------------------------------------------------------
 * Replace Read More Link
 *------------------------------------------------------------------------ */
function gm925_read_more() {
  $moreText = Read More;
  return '<a class="more-link" href="' . get_permalink() . '">' . $moreText . '</a>';
}
add_filter( 'the_content_more_link', 'gm925_read_more' );

/*--------------------------------------------------------------------------
 * Replace excerpt more link
 *------------------------------------------------------------------------ */
function gm925_excerpt_more() {
	global $post;
  $moreText = Read More;
	return '... <a href="'. get_permalink($post->ID) . '">' . $moreText . '</a>';
}
add_filter('excerpt_more', 'gm925_excerpt_more');
