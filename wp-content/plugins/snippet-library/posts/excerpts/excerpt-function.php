<?php 
/*
|--------------------------------------------------------------------------
| Get excerpt of any custom field limited to defined length
|--------------------------------------------------------------------------
*/
function gm925_get_excerpt($field, $count, $post_id = '') {
  if ( $post_id == '' ) {
    global $post;
    $post_id = $post->ID;
  }
  $excerpt = get_post_field($field, $post_id);
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  return $excerpt;
}
