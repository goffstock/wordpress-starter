<?php

/*--------------------------------------------------------------------------
 * Allow for custom single templates for specific posts. 
 * Format: single-post_type-slug.png
 *------------------------------------------------------------------------ */
function gm925_get_custom_template( $template ) {
    global $post;
    if ( $post->post_type === '[post_type]' ) {
        $locate_template = locate_template( "single-[post_type]-{$post->post_name}.php" );
        if ( ! empty( $locate_template ) ) {
            $template = $locate_template;
        }
    }
    return $template;
}
add_filter( 'single_template', 'gm925_get_custom_template' );

