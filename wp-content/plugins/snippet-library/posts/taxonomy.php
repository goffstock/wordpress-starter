<?php 


/*--------------------------------------------------------------------------
 * 
 *------------------------------------------------------------------------ */
function gm925_get_post_terms($postCats) {
  $returnArray = [];
  $returnArray['name'] = '';
  $returnArray['slug'] = '';
  $returnArray['filter'] = '';
  if ( $postCats != '' ) {
    $postLen = count($postCats);
    $postCatList = '';
    $postCatTagList = '';
    $postCatFilter = [];
    $i = 0;
    foreach( $postCats as $category ) {
      if ( $category->slug != 'blog' && $category->slug != 'uncategorized' ) {
        if ( $i == $postLen - 1 ) {
          $postCatList .= $category->name;
          $postCatTagList .= $category->slug;
          $postCatFilter[] = $category->name;
        } else {
          $postCatList .= $category->name . ' / ';
          $postCatTagList .= $category->slug . ' ';
          $postCatFilter[] = $category->name;
        }
      }
      $i++;
    }
    $returnArray['name'] = $postCatList;
    $returnArray['slug'] = $postCatTagList;
    $returnArray['filter'] = $postCatFilter;
  }
  return $returnArray;
}



/*--------------------------------------------------------------------------
 * Get list of tags & sort alphabetically
 *
 * Returns array of alphabetically sorted tags
 *
 *
 * $post_type = post,
 * $tax = post_tag,
 * $sort = SOgm925REGULAR, SOgm925NUMERIC, SOgm925STRING, SOgm925LOCALE_STRING, SOgm925NATURAL, SOgm925FLAG_CASE
 *
 * Usage:
 *
 * tags_list("post", "post_tag", "SOgm925NUMERIC");
 *
 * echo '<ul>';
 *   echo '<li><h3>Filter Projects:</h3></li>';                                              // Echo title li
 *   echo '<li class="js-unfiltered"><button data-filter="*">Remove Filter</button></li>';   // Echo Unfilter li (isotope)
 *   foreach ( $tags as $tag ) {                                                             // Echo li for each tag (isotope)
 *     echo '<li class="o-filter-' . $tag->slug .'">';
 *       echo '<button data-filter=".' . $tag->slug . '">' . $tag->icon . '</button>';
 *       echo '<span class="c-tooltip">' . $tag->name . '</span>';
 *     echo '</li>';
 *   }
 * echo '</ul>';
 *
 *------------------------------------------------------------------------ */

 function gm925_tags_list($post_type="post", $tax="post_tag" $sort="SOgm925REGULAR") {
  $terms = get_terms( array(
    'post_type' = $post_type,
    'taxonomy' => $tax,
    'hide_empty' => true,
  ));

  $tags = array();
  foreach($terms as $term) {
    $order = get_field('order', $term);
    $tags[$order] = (object) array(
      'name' => $term->name,
      'slug' => $term->slug,
      'term_id' => $term->term_id,
      'icon' => get_field('icon', $term, false)
    );
  }

  ksort( $tags, $sort );
  return $tags;
 }
