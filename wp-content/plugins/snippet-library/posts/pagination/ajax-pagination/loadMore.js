
// Load more button on archive pages
if ( document.getElementById( 'archive-footer' ) ) {
  loadMoreArticleWatcher();
}


//---------------------------------------------------------------------|
//  Load more articles on button click in archives
//---------------------------------------------------------------------|
function loadMoreArticleWatcher() {
  document.getElementById("load-more").addEventListener('click', function(e) {
    // Variables to store elements we'll be using
    var container = document.getElementById('article-content');
    var pagArea = document.getElementById('archive-footer');
    var button = document.getElementById("load-more");
    var baseUrl = button.getAttribute('data-link');
    var nextPage = parseInt(button.getAttribute('data-next'));
    var maxPage = parseInt(button.getAttribute('data-max'));
    var link = baseUrl + '/' + nextPage;

    // Update button text
    button.innerHTML = 'Loading More Posts...';

    // Ajax request to get next page's posts...
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function(data) {
     if (this.readyState == 4 && this.status == 200) {
        var response = data.target.responseText;
        var parser = new DOMParser();
        var xmlDoc = parser.parseFromString(response, "text/html");
        // Get only the posts on the page as an array
        var posts = xmlDoc.querySelectorAll('.archive__article');
        // Loop through next page posts & append to current page
        for (var i = 0; i < posts.length; i++) {
          container.appendChild(posts[i]);
        }
     }
    };
    // Send the Ajax request
    httpRequest.open('GET', link);
    httpRequest.send();

    // Update next page attribute on button
    button.setAttribute('data-next', nextPage + 1);
    // Hide next button if there are no more posts 
    console.log(nextPage, maxPage);
    if ( nextPage + 1 > maxPage ) {
      pagArea.classList.add('is-hidden');
    }

    // Update button text once everything else is done
    button.innerHTML = 'Load More...';

  }, false);
}