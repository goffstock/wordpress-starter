<?php 
global $wp_query;
if ( $wp_query->max_num_pages > 1 ) {
  $link = rtrim(get_next_posts_page_link(), '/\\');
  $pos = strrpos($link, '/');
  $page = $pos === false ? $link : substr($link, $pos + 1);
  $baseUrl = $pos === false ? $link : substr($link, 0, $pos);
?>
  <footer id="archive-footer" class="archive__footer">
    <button id="load-more" class="btn--rev" data-link="<?php echo $baseUrl; ?>" data-next="<?php echo $page; ?>" data-max="<?php echo $wp_query->max_num_pages; ?>" aria-label="load more posts">Load More...</button>
  </footer><!-- .archive__footer -->
<?php } ?>
