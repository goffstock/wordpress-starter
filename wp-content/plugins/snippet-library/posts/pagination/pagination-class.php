<?php
/*--------------------------------------------------------------------------
 * Add class to next / prev buttons
 *------------------------------------------------------------------------ */
function gm925_next_link_attr($output) {
    $btnClass = 'class="btn--next"';
    return str_replace('<a href=', '<a '. $btnClass .' href=', $output);
}

function gm925_prev_link_attr($output) {
    $btnClass = 'class="btn--prev"';
    return str_replace('<a href=', '<a '. $btnClass .' href=', $output);
}
add_filter('next_post_link', 'gm925_next_link_attr');
add_filter('previous_post_link', 'gm925_prev_link_attr');
