<?php
/*--------------------------------------------------------------------------
 * Sort post type archive to order by order field
 *------------------------------------------------------------------------ */
function gm925_change_order($query) {
    if ( is_post_type_archive( 'post' ) ):
       $query->set( 'order', 'ASC' );
       $query->set( 'orderby', 'menu_order' );
    endif;
};
add_action( 'pre_get_posts', 'gm925_change_order');
