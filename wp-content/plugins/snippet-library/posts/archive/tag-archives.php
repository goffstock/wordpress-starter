<?php 

/*--------------------------------------------------------------------------
 * Add Tag Archives for Custom Post Types
 *------------------------------------------------------------------------ */
function gm925_post_type_tags_fix($request) {
    if ( isset($request['tag']) && !isset($request['post_type']) )
    $request['post_type'] = 'any';
    return $request;
}
add_filter('request', 'gm925_post_type_tags_fix');
