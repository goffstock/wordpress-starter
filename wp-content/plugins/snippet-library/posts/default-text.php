<?php
/*--------------------------------------------------------------------------
 * Default Post Text
 *------------------------------------------------------------------------ */
function gm925_default_text( $content ) {
  if ( get_post_type() == 'post' ) {
    $content = "If you enjoyed this post, make sure to subscribe to my rss feed.";
    return $content;
  }
}
add_filter('default_content', 'gm925_default_text');
