<?php
/*------------------------------------*
* PHP Class
*------------------------------------*/

	class person {
    // property (variable)
    var $name;
    // properties with access modifiers
    // Public has no restrictions
    public $height;
    // Protected can only be accessed by the same class & those derived from that class
    protected $social_insurance;
    // Private can only be accessed by the same class
    private $pinn_number;
    // Constructor method (Function)
    function __construct($persons_name) {
      $this->name = $persons_name;
    }
    // Setter method (Function)
    function set_name($new_name) {
      $this->name = $new_name;
    }
    // Getter method (Function)
    function get_name() {
      return $this->name;
    }
	}
  // Extend parent class to child class
    // Has all the same methods & peroperties (just protected & Public) as parent
    class employee extends person {
    	function __construct($employee_name) {
    		$this->set_name($employee_name);
    	}
    }
  // Extend parent class & override methods
    class volunteer extends person {
      protected function set_name($new_name) {
        if ($new_name != "John Smith") {
          $this->name = $new_name;
        } else {
          // <object>:: calls the method from the parent class
          person::set_name($new_name);
          // Or use this to call the parent:
          parent::set_name($new_name);
        }
        function __construct($volunteer_name){
          $this->set_name($volunteer_name);
        }
      }
    }
?>

<?php
// Include class file in your site file(s):
?>

<?php include("class_lib.php"); ?>

<?php
  // Use in your site file(s):
    // Instantiate Object
    $Joe = new person();
    $Bill = new person;
    // Set object properties
    $Joe->set_name("Joe Smith");
    $Bill->set_name("Bill Smoth");
    // Or instantiate & set properties using the constructor function...
    $Joe = new person("Joe Smith");
    // Instantiate child Class
    $Fred = new employee("Fred Smith");
    echo "---> " . $Fred->get_name();
    // Access object data
    echo "Joe's full name: " . $Joe->get_name();
    echo "Bill's full name: " . $Bill->get_name();
?>
