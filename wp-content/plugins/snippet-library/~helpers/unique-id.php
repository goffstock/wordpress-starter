<?php
// Generate Unique ID

// Unique ID
echo uniqid();
  // prints 4bd67d6cd8b8f

// with prefix
echo uniqid('foo_');
  // prints foo_4bd67d6cd8b8f

// with more entropy
echo uniqid('',true);
  // prints 4bd67d6cd8b926.12135106

// with prefix & more entropy
echo uniqid('bar_',true);
  //prints bar_4bd67da367b650.43684647
