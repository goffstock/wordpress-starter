<?php
/*--------------------------------------------------------------------------
 * Conditional arguments tax_query in WP_Query
 *------------------------------------------------------------------------ */
$tax_query = array('relation' => 'OR');
array_push($tax_query, array(
	'taxonomy' => 'tax_slug',
	'field' => 'tax_field(slug, name, etc)',
	'terms' => 'tax_term'
));
if( conditional ) {
	array_push($tax_query, array(
			'taxonomy' => 'tax_slug',
		  'field' => 'tax_field(slug, name, etc)',
		  'terms' => 'tax_term'
	));
}
$args = array (
	'post_type' => 'post',
	'tax_query' => $tax_query,
);
$the_query = new WP_Query( $sidebar_args );
