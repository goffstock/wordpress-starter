
/*==========================================================
    Global Variables 
============================================================*/
var i;
var cartType = document.body.classList;
if ( document.getElementsByClassName('js-prod').length > 0 ) {
    var products = document.getElementsByClassName('js-prod');
    var qtyButton = document.getElementsByClassName('products')[0].getElementsByClassName('td-quantity-button');
    var addButton = document.getElementsByClassName('products')[0].getElementsByClassName('add-to-cart-custom');
} else if ( document.getElementsByClassName('js-auto').length > 0 ) {
    var products = document.getElementsByClassName('js-auto');
    var qtyButton = document.getElementsByClassName('products')[0].getElementsByClassName('td-quantity-button');
    var addButton = null;
}


var cartQtyButton = document.getElementById('sidecart').getElementsByClassName('td-quantity-button');
var cartQtyInput = document.getElementById('sidecart').getElementsByClassName('qty');

var clrButton = document.getElementsByClassName('clear-sidecart');
var overlay = document.getElementsByClassName('darken-bg-mobile')[0];
var sideCart =  document.getElementById('sidecart');


var baseUrl = document.body.getAttribute('data-path');
var url = new URL(baseUrl);
var id = url.searchParams.get("add-to-cart");



/*==========================================================
    Functions 
============================================================*/

function cartWatch() {
    if ( cartQtyButton.length > 0 ) {
        for (i = 0; i < cartQtyButton.length; i++) {
            cartQtyButtonWatch(cartQtyButton[i]);
        }
    } 
    if ( cartQtyInput.length > 0 ) {
        for (i = 0; i < cartQtyInput.length; i++) {
            cartQtyInputWatch(cartQtyInput[i]);
        }
    } 
}


/*
 *  Find closest matching ancestor
 */
function findAncestor(el, sel) {
    while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el,sel)));
    return el;
}

/* Open sidecart */
function openSideCart() {
    if ( !document.body.classList.contains('open-sidecart')) {
        document.body.classList.add('open-sidecart');
    }
}

/* Disable Cart Interaction: To prevent multiple ajax requests */
function enableAjaxPause() {
    if ( !document.body.classList.contains('is-ajax-working') ) {
        document.body.classList.add('is-ajax-working');
    }
}

/* Enable Cart Interaction: To prevent multiple ajax requests */
function disableAjaxPause() {
    if ( document.body.classList.contains('is-ajax-working') ) {
        document.body.classList.remove('is-ajax-working');
    }
}

/*
 *  
 */
function qtyURL() {
    for (i = 0; i < productQuantity.length; i++) { 
        var indexStart = AddToCartButton[i].href.indexOf("=");
        var indexEnd = AddToCartButton[i].href.indexOf("&");
        if(indexEnd == -1){
            indexEnd = AddToCartButton[i].length;
        }
        var id = AddToCartButton[i].href.slice(indexStart + 1, indexEnd);
        AddToCartButton[i].href = baseURL + '?add-to-cart=' +  id + '&quantity=' + productQuantity[i].value;
    }
}

/*
 *  Check quantity of item chosen on store page. If > 0, enable button & change styling
 */

function checkQty(product) {
    if ( addButton != null ) {
        var button = product.getElementsByClassName('add-to-cart-custom')[0];
        var quantity = product.getElementsByClassName('qty')[0].value;
        if ( quantity > 0 ) {
            button.classList.remove('disabled');
        } else {
            button.classList.add('disabled');
        }
    }
}

/*
 *  Loop through single-purchase products on page
 */
function checkProducts() {
    for ( i = 0; i < products.length; i++ ) {
        checkQty(products[i]);
    }
}

/*
 *  Set up products & product fields (called on initial page load);
 */
function initProducts() {
    if ( addButton != null ) {
        checkProducts();
        for ( i = 0; i < products.length; i++ ) {
            var productButton = products[i].getElementsByClassName('add-to-cart-custom')[0];
            productButton.addEventListener('click', function(event) {
                event.preventDefault();
                enableAjaxPause();
                var parent = findAncestor(this, '.js-prod');
                addToCart(parent);
            });
        }
    }

    // Repurposed old jquery quantity watcher to apply only to product page +/- buttons
    jQuery('.products .td-quantity-button').on('click', function () {
        var $this = jQuery(this);
        var $input = $this.parent().find('input');
        var $quantity = $input.val();
        var $new_quantity = 0;
        if ($this.hasClass('plus')) {
            $new_quantity = parseFloat($quantity) + 1;
        } else {
            if ($quantity > 0) {
                $new_quantity = parseFloat($quantity) - 1;
            }
        }
        $input.val($new_quantity);
        if ( document.getElementsByClassName('js-auto').length > 0 ) {
            var el = findAncestor(this, '.js-auto');
            updateSubscribePreview(el);
        }
    });
}



// Repurposed old jquery quantity watcher to apply only to product page +/- buttons
function cartQtyButtonWatch(el) {
    var cartProduct = findAncestor(el, '.js-cart-prod');
    cartProduct.classList.remove('is-qty-inactive');
    el.addEventListener('click', function(event) {
        if ( !document.body.classList.contains('is-ajax-working') ) {
            enableAjaxPause();
            var $this = jQuery(this);
            var $input = $this.parent().find('input');
            var $newDisplayVal = 0;
            var $quantity = $input.val();
            var dir = '';
            if ( event.target.classList.contains('plus') ) {
                $newDisplayVal = parseFloat($quantity) + 1;
                dir = 'plus';
            } else {
                if ($quantity > 0) {
                    $newDisplayVal = parseFloat($quantity) - 1;
                }
                $newDisplayVal = Math.max( parseFloat($quantity) - 1, 0);
                dir = 'min';
            }
            changeCartQty(cartProduct, dir);
        }
    });
}

function cartQtyInputWatch(el) {
    el.addEventListener('blur', function(event) {
        enableAjaxPause();
        var $this = jQuery(this);        
        var $input = $this.val();
        var $quantity = parseInt($input);
        if ( $quantity >= 0 ) {
            var cartProduct = findAncestor(this, '.js-cart-prod');
            changeCartQty(cartProduct, $quantity);
        }
    });
}



/*==========================================================
    Cart Functions & Ajax
============================================================*/
/* Build Submission URL */
function buildAddURL(qtyField) {
    var urlId = qtyField.getAttribute('data-id');
    var urlQty = qtyField.value;
    var addUrl = baseUrl;
    addUrl += '?add-to-cart=' + urlId + '&quantity=' + urlQty;
    return addUrl;
}

/* Build URL with ? */
function buildUrl(addon) {
    var buildUrl = baseUrl;
    buildUrl += '?' + addon;
    return buildUrl;
}

/* Empty shopping cart */
function clearCart() {
    var url = buildUrl('clear-cart=#');
    var apiRequest = new XMLHttpRequest();
    apiRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            location.reload();
            //updateCart();
        }
   };
   apiRequest.open("POST", url, true);
   apiRequest.send();
}

function clearAndAdd(el) {
    var url = buildUrl('clear-cart=#');
    var apiRequest = new XMLHttpRequest();
    apiRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            addtoCartRequest(el);

            //updateCart();
        }
   };
   apiRequest.open("POST", url, true);
   apiRequest.send();

}


/* Update cart item count in header of site */
function updateCartCount() {
    // Target all count elements (all, not just the first in case any future templates display this count)
    var cartCountEls = document.getElementsByClassName('cart-contents-nav');
    // JQuery ajax call...
    ( function ( $ ) {
     "use strict";
    // Define the PHP function to call from here. See inc/ajax.php for all php ajax functions
     var data = {
       'action': 'get_cart_count'
     };
     $.post(
       woocommerce_params.ajax_url, // The AJAX URL
       data, // Send our PHP function
       function(response){
        for (i = 0; i < cartCountEls.length; i++) {
            // Update all instances where total quantity is displayed.
            var countEl = cartCountEls[i].getElementsByClassName('cart-contents-nav-count')[0];
            countEl.childNodes[0].nodeValue = response / 10;
            // If the cart value is zero, hide the qty circle
            if ( response > 0 ) {
                cartCountEls[i].classList.remove("is-empty");
            } else { 
                cartCountEls[i].classList.add("is-empty");
            }
        }
       }
     );
    // Close anon function.
    }( jQuery ) );
}

/* Update Shopping cart on change */
function updateCart() {
    // JQuery ajax call...
    ( function ( $ ) {
        "use strict";
        // Define the PHP function to call from here. See inc/ajax.php for all php ajax functions
        var data = {
            'action': 'get_cart_data'
        };
        $.post(
            woocommerce_params.ajax_url, // The AJAX URL
            data, // Send our PHP function
            function(response){
             $('#form-wrap').html(response); // Repopulate the specific element with the new content
                updateCartCount();
                cartWatch();
                disableAjaxPause();
            }
         );
    // Close anon function.
    }( jQuery ) );

}


function addtoCartRequest(el) {
    var productQty = el.getElementsByClassName('qty')[0];
    var url = buildAddURL(productQty);
    var addedId = productQty.getAttribute('data-id');
    // Create Ajax Request
    var apiRequest = new XMLHttpRequest();
    // Create event handler to run after successful response
    apiRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Parse data as json
            //var response = JSON.parse(this.responseText);
            // Pass parsed data to displayData function
            //displayData(response);
            updateCart();
            openSideCart();
        }
   };

   // Configure API request
   apiRequest.open("POST", url, true);
   // Send API request
   apiRequest.send();
}

/* Add item to cart */
function addToCart(el) {
    if ( el.classList.contains('js-miles') && cartType.contains('cart-has-normal') ) {
        if ( confirm("Cannot Purchase Point Pricing Product with Normal product. Clear cart and add new item?" ) ) {
            //clearCart();
            clearAndAdd(el);
        } else {
            return false;
        }
    } else if ( el.classList.contains('js-normal') && cartType.contains('cart-has-miles')) {
        if ( confirm("Cannot Purchase Normal Pricing Product with Point product. Clear cart and add new item?" ) ) {
            clearCart();
        } else {
            return false;
        }
    } else {
        addtoCartRequest(el);
    }
}

function changeCartQty(el, dir) {
    var origQty = parseInt(el.getAttribute('data-qty'));
    var key = el.getAttribute('data-key');
    var newQty = '';
    if ( dir == 'plus' ) {
        newQty = origQty + 1;
    } else if ( dir == 'min' ) {
        newQty = origQty - 1;
    } else if ( Number.isInteger(dir) ) {
        newQty = dir;
    } else {
        console.error('invalid quantity');
        return;
    }
    jQuery.ajax({
        type: 'POST',
        url: baseUrl + '/wp-admin/admin-ajax.php',
        data: {
            action: 'update_cart_item',
            key: key,
            qty: newQty
        },
        success: function(data, textStatus, XMLHttpRequest) {
            updateCart();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        },
        complete: function(XMLHttpRequest, textStatus) {
            //disableAjaxPause();
        }
    });

}

function updateSubscribePreview(el) {
    var id = el.getAttribute('data-id');
    var qty = el.getElementsByClassName('qty')[0].value;

    var target = document.getElementById('prod-id-' + id);
    var qtyField = target.getElementsByClassName('js-auto-qty')[0];
    var priceField = target.getElementsByClassName('js-auto-price')[0];


    if ( qty > 0 ) {
        var priceEach = priceField.getAttribute('data-discount');
        var priceTotal = priceEach * qty * 1.17657;
        target.classList.remove('hidden');
        qtyField.childNodes[0].nodeValue = qty;
        priceField.childNodes[0].nodeValue = priceTotal.toFixed(2);
    } else {
        target.classList.add('hidden');
        qtyField.childNodes[0].nodeValue = 0;
        priceField.childNodes[0].nodeValue = 0.00;
    }
    updateSubscribeTotals();

}

function updateSubscribeTotals() {
    var fullPriceField = document.getElementById('js-auto-full');
    var discountField = document.getElementById('js-auto-discount');
    var totalField = document.getElementById('js-auto-total');
    var prices = document.getElementsByClassName('js-auto-price');
    var sum = parseFloat(0.00);
    for ( var i = 0; i < prices.length; i++ ) {
        sum += parseFloat(prices[i].childNodes[0].nodeValue);
    }

    var discounted = sum / 1.17657;
    discounted = discounted.toFixed(2);
    sum = sum.toFixed(2);

    fullPriceField.childNodes[0].nodeValue = sum;
    discountField.childNodes[0].nodeValue = sum - discounted;
    totalField.childNodes[0].nodeValue = discounted;
}

function addSubscription() {
    var activeProducts = document.getElementsByClassName('js-auto-row');
    var redirect = baseUrl + '/my-account/';
    var productArray = new Array();

    if ( activeProducts.length == 0 ) {
        console.log('no products');
        return;
    }


    for ( i = 0; i < activeProducts.length; i++ ) {
        if ( !activeProducts[i].classList.contains('hidden') ) {
            var productInfo = activeProducts[i].getElementsByClassName('js-auto-qty')[0];
            var id = productInfo.getAttribute('data-id');
            var qty = parseInt(productInfo.childNodes[0].nodeValue);
            productArray.push(id + ':' + qty);

        }
        if ( document.getElementById('js-subscribe').classList.contains('logged-out')) {
            window.location = redirect;
        }
    }

    if ( productArray.length > 0 ) {
        productArray.join();

        var url = baseUrl;
        url += '/?add-to-cart=' + productArray;
        console.log(url);

        // Create Ajax Request
        var apiRequest = new XMLHttpRequest();
        // Create event handler to run after successful response
        apiRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // Parse data as json
                //var response = JSON.parse(this.responseText);
                // Pass parsed data to displayData function
                //displayData(response);
                updateCart();
                openSideCart();
            }
        };
       // Configure API request
       apiRequest.open("POST", url, true);
       // Send API request
       apiRequest.send();
    }
}




/*==========================================================
    Initialize Site 
============================================================*/

/* If there are products on page, run initProducts */
if ( products ) {
    initProducts();
}

/* Clear cart button event watcher */
if ( clrButton.length > 0 ) {
    for (i = 0; i < clrButton.length; i++) {
        clrButton[i].addEventListener('click', function() {
            if ( confirm("Clear All Items?" ) ) {
                clearCart();
            } else {
                return false;
            }
        });
    }
}

/* If quantity buttons exist on page, update 'add to cart' buttons on click */
if ( qtyButton ) {
    for (i = 0; i < qtyButton.length; i++) {
        qtyButton[i].addEventListener('click', function(event) {
            var product = findAncestor(this, '.js-prod');
            checkQty(product);
        });
    }
}

if ( document.getElementById('js-subscribe') ) {
    document.getElementById('js-subscribe').addEventListener('click', addSubscription);
}

cartWatch();

/* If overlay div exists on page, set event listener to close it when it's click */
if ( overlay ) {
    overlay.addEventListener('click', function(event) {
        if(document.body.classList.contains('open-sidecart')) {
            document.body.classList.remove('open-sidecart');
        } 
    });
}

