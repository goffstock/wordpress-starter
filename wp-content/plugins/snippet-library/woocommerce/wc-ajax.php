<?php 

/* Get total number of items in shopping cart */
function get_cart_count() {
  $count = WC()->cart->get_cart_contents_count();
  echo (int)$count; 
}
add_filter( 'wp_ajax_nopriv_get_cart_count', 'get_cart_count' );
add_filter( 'wp_ajax_get_cart_count', 'get_cart_count' );


/* Echo cart template */
function get_cart_data() {
  echo wc_get_template( 'template-parts/sidecart-loop.php' );
  die();
}
add_filter( 'wp_ajax_nopriv_get_cart_data', 'get_cart_data' );
add_filter( 'wp_ajax_get_cart_data', 'get_cart_data' );


/* Update cart quantity */
function update_cart_item() {
  $key = $_POST['key'];
  $qty = $_POST['qty'];
  global $woocommerce;
  $woocommerce->cart->set_quantity($key, $qty, true);
  die();
}
add_action( 'wp_ajax_update_cart_item', 'update_cart_item' );
add_action('wp_ajax_nopriv_update_cart_item', 'update_cart_item');


?>