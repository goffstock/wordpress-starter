<?php
/*--------------------------------------------------------------------------
 * Register Post Types
 *-------------------------------------------------------------------------*/


function portfolio() {

	$labels = array(
		'name'                  => _x( 'Portfolio', 'Post Type General Name', 'site-functionality' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'site-functionality' ),
		'menu_name'             => __( 'Portfolio', 'site-functionality' ),
		'name_admin_bar'        => __( 'Portfolio', 'site-functionality' ),
		'archives'              => __( 'Item Archives', 'site-functionality' ),
		'attributes'            => __( 'Item Attributes', 'site-functionality' ),
		'parent_item_colon'     => __( 'Parent Item:', 'site-functionality' ),
		'all_items'             => __( 'All Items', 'site-functionality' ),
		'add_new_item'          => __( 'Add New Item', 'site-functionality' ),
		'add_new'               => __( 'Add New', 'site-functionality' ),
		'new_item'              => __( 'New Item', 'site-functionality' ),
		'edit_item'             => __( 'Edit Item', 'site-functionality' ),
		'update_item'           => __( 'Update Item', 'site-functionality' ),
		'view_item'             => __( 'View Item', 'site-functionality' ),
		'view_items'            => __( 'View Items', 'site-functionality' ),
		'search_items'          => __( 'Search Item', 'site-functionality' ),
		'not_found'             => __( 'Not found', 'site-functionality' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'site-functionality' ),
		'featured_image'        => __( 'Featured Image', 'site-functionality' ),
		'set_featured_image'    => __( 'Set featured image', 'site-functionality' ),
		'remove_featured_image' => __( 'Remove featured image', 'site-functionality' ),
		'use_featured_image'    => __( 'Use as featured image', 'site-functionality' ),
		'insert_into_item'      => __( 'Insert into item', 'site-functionality' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'site-functionality' ),
		'items_list'            => __( 'Items list', 'site-functionality' ),
		'items_list_navigation' => __( 'Items list navigation', 'site-functionality' ),
		'filter_items_list'     => __( 'Filter items list', 'site-functionality' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'site-functionality' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'page-attributes', ),
		'taxonomies'            => array( 'portfolio_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
    'rewrite'            => array( 'slug' => 'portfolio' ),

	);
	register_post_type( 'portfolio', $args );
}
add_action( 'init', 'portfolio', 0 );
*/
