<?php
/*--------------------------------------------------------------------------
 * Taxonomy
 *-------------------------------------------------------------------------*/



// Register Custom Taxonomy
/*
function portfolio_cat() {

	$labels = array(
		'name'                       => _x( 'Portfolio Categories', 'Taxonomy General Name', 'site-functionality' ),
		'singular_name'              => _x( 'Portfolio Category', 'Taxonomy Singular Name', 'site-functionality' ),
		'menu_name'                  => __( 'Portfolio Category', 'site-functionality' ),
		'all_items'                  => __( 'All Items', 'site-functionality' ),
		'parent_item'                => __( 'Parent Item', 'site-functionality' ),
		'parent_item_colon'          => __( 'Parent Item:', 'site-functionality' ),
		'new_item_name'              => __( 'New Item Name', 'site-functionality' ),
		'add_new_item'               => __( 'Add New Item', 'site-functionality' ),
		'edit_item'                  => __( 'Edit Item', 'site-functionality' ),
		'update_item'                => __( 'Update Item', 'site-functionality' ),
		'view_item'                  => __( 'View Item', 'site-functionality' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'site-functionality' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'site-functionality' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'site-functionality' ),
		'popular_items'              => __( 'Popular Items', 'site-functionality' ),
		'search_items'               => __( 'Search Items', 'site-functionality' ),
		'not_found'                  => __( 'Not Found', 'site-functionality' ),
		'no_terms'                   => __( 'No items', 'site-functionality' ),
		'items_list'                 => __( 'Items list', 'site-functionality' ),
		'items_list_navigation'      => __( 'Items list navigation', 'site-functionality' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_cat', array( 'portfolio' ), $args );

}
add_action( 'init', 'portfolio_cat', 0 );
*/
