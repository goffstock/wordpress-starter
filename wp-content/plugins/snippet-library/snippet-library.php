<?php

/*
Plugin Name: Snippet Library
Plugin URI: http://www.mortstock.com/
Description: Library of useful WordPress snippets, functions, hooks, etc
Author: goffstock
Author URI: http://www.mortstock.com/
*/

if ( !function_exists('do_action') ) {
  exit();
}

/*
require_once( plugin_dir_path( __FILE__ ) . 'includes/meta.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/post-types.php');
require_once( plugin_dir_path( __FILE__ ) . 'includes/taxonomy.php');
require_once( plugin_dir_path( __FILE__ ) . 'includes/custom-fields.php' );
foreach (glob(plugin_dir_path( __FILE__ ) . 'includes/functions/*.php') as $filename) {
  require_once $filename;
}
foreach (glob(plugin_dir_path( __FILE__ ) . 'includes/hooks/*.php') as $filename) {
  require_once $filename;
}
foreach (glob(plugin_dir_path( __FILE__ ) . 'includes/shortcodes/*.php') as $filename) {
  require_once $filename;
}
*/