<?php
/*--------------------------------------------------------------------------
 * Hide Login Errors
 *------------------------------------------------------------------------ */
function gm925_login_error(){
  return 'Login Error.';
}
add_filter( 'login_errors', 'gm925_login_error' );
