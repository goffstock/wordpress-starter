<?php

/*--------------------------------------------------------------------------
 * Add a favicon to your site
 *------------------------------------------------------------------------ */
function gm925_favicon() {
  $icon = "assets/img/icons/favicon.ico";
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl') . $icon . '"/>';
}
add_action('wp_head', 'gm925_favicon');
