<?php 
// Include CSS & JS files
  function gm925scripts() {
    // get google CDN jQuery rather than local copy
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js' , array() , '2.2.2', false);
    // Theme Styles
    wp_enqueue_style( 'main-style', get_template_directory_uri().'/assets/css/style.css' );
    wp_enqueue_style( 'slick-style', get_template_directory_uri().'/assets/vendor/slick/slick.css' );
    // Fonts
    wp_enqueue_style( 'main-style', 'https://fonts.googleapis.com/css?family=Boogaloo' );
    wp_enqueue_style( 'main-style', 'https://fonts.googleapis.com/css?family=Raleway:700' );
    // Theme Scripts
    wp_enqueue_script( 'theme-scripts', get_template_directory_uri().'/assets/js/script.min.js', array( 'jquery','slick-scripts' ), '1.0' , true );
    wp_enqueue_script( 'slick-scripts', get_template_directory_uri().'/assets/vendor/slick/slick.min.js', array( 'jquery' ), '1.0' , true );
  }
  add_action('wp_enqueue_scripts' , 'gm925scripts');
