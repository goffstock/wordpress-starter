<?php
/*--------------------------------------------------------------------------
 * Add additional Mime Types
 *------------------------------------------------------------------------ */
function gm925_myme_types($mime_types){
  $mime_types['svg'] = 'image/svg+xml';
  $mime_types['psd'] = 'image/vnd.adobe.photoshop';
  return $mime_types;
}
add_filter('upload_mimes', 'gm925_myme_types', 1, 1);
