<?php

/*--------------------------------------------------------------------------
 * Return value of "redirect_link" custom field if exists,
 * otherwise return permalink.
 *-------------------------------------------------------------------------*/
function gm925_get_custom_link($linkField) {
  global $post;
  if (get_post_meta($post->ID, $linkField, true)) {
    return get_post_meta($post->ID, $linkField, true);
  } else {
    get_permalink();
  }
}

/*--------------------------------------------------------------------------
 * Check ACF Field. Return default field value if empty
 *-------------------------------------------------------------------------*/
function gm925_checkField($field, $defaultText) {
  if ( get_field($field) == '' || get_field($field) == '#' ) {
    return $defaultText;
  } else {
    return get_field($field);
  }
}

/*--------------------------------------------------------------------------
 * Get meta data for archive single posts.
 *-------------------------------------------------------------------------*/
function gm925_get_meta($field) {
  global $post;
  if (get_post_meta($post->ID, $field, true)) {
    return get_post_meta($post->ID, $field, true);
  }
}

