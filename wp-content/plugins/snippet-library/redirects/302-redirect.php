<?php
// 302 Redirect Function

function redirect_to($new_location) {
  header("Location: " . $new_location);
  exit;
}

// Check logged in state to determine if you should redirect
$logged_in = $_GET['logged_in'];
if ($logged_in == "1") {
  redirect_to("dashboard.html");
} else {
  redirect_to("login.html");
}
