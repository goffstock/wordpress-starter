<?php
class GRM77_Sub_Menu_Icon_Walker extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth=0, $args=[], $id=0) {
		$output .= "<li class='" .  implode(" ", $item->classes) . "'>";
	
		$output .= '<span>';

		if ( $item->url ) {
			$output .= '<a href="' . $item->url . '">';
		} else {
			$output .= '<a>';
		}
	
		$output .= $item->title;
		$output .= '</a>';
	
		if ($args->walker->has_children) {
			$output .= '<button class="btn--submenu" type="button" name="Toggle Submenu" aria-label="Toggle Submenu">+</button>';
		}
		$output .= '</span>';
	}
}