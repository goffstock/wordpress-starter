<?php 
// https://developer.wordpress.org/block-editor/developers/block-api/block-templates/

add_action( 'init', 'wec_register_template' );
function wec_register_template() {
    $post_type_object = get_post_type_object( 'page' );
    $post_type_object->template = array(
        array( 'core/image' ),
	);
}