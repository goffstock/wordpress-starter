<?php
// Add svg to uploads
function grm77_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'grm77_mime_types');

// Get image URL from Image ID
function getImageUrl($image_id,$image_size) {
  return wp_get_attachment_image_url( $image_id, $image_size );
}

// Add function to get responsive images
function grm77_acf_responsive_image($image_id,$image_size,$max_width){
	// check the image ID is not blank
	if( $image_id !== '' ) {
		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );
		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
		// Get Alt tag
		$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
		// generate the markup for the responsive image
		echo 'alt="' . $image_alt . '" src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
	}
}

add_filter( 'max_srcset_image_width', 'grm77_max_srcset_image_width', 10 , 2 );
// set the max image width 
function grm77_max_srcset_image_width() {
    return 2200;
}

// Add new image sizes
add_image_size( 'img-med-2', 600, 600 );
add_image_size( 'img-med-3', 800, 800 );
add_image_size( 'img-xl', 1500, 1500 );
