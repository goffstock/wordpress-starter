<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package GRM77_Theme
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function grm77_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'grm77_body_classes' );

// Get Source for ACF Meta
function grm77_get_meta_source() {
	if ( is_post_type_archive('team') || is_singular('team') ) {
	  return 'options';
	}
	if ( is_post_type_archive('post') || is_singular('post') ) {
	  return get_option( 'page_for_posts' );
	}
	return get_queried_object_id();
}

function grm_dump($data) {
	echo '<pre>';
		var_dump($data);
	echo '</pre>';
}


function grm77_linkify_text($text) {
	$text = make_clickable($text);
	$text = links_add_target( $text, '_blank', array('a') );
	return $text; 
}