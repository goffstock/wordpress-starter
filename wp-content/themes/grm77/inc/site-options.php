<?php


if( function_exists('acf_add_options_page') ) {

	$parent = acf_add_options_page(array(
		'page_title'    => 'Site Settings',
		'menu_title'    => 'Site Settings',
		'menu_slug'     => 'site-settings',
		'capability'    => 'edit_posts',
		'redirect'      => false
	));

	// add Header subpage
	acf_add_options_sub_page(array(
		'page_title'    => 'Header Settings',
		'menu_title'    => 'Header',
		'menu_slug'     => 'header-settings',
		'parent_slug'   => $parent['menu_slug'],
	));

	// add Footer subpage
	acf_add_options_sub_page(array(
		'page_title'    => 'Footer Settings',
		'menu_title'    => 'Footer',
		'menu_slug'     => 'footer-settings',
		'parent_slug'   => $parent['menu_slug'],
	));
}

