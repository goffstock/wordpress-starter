//----------------------------------------------|
// Functions
//----------------------------------------------|

// Debounce
function debounce( func, wait, immediate ) {
	var timeout;
	return function () {
		var context = this,
			args = arguments;
		var later = function () {
			timeout = null;
			if ( !immediate ) func.apply( context, args );
		};
		var callNow = immediate && !timeout;
		clearTimeout( timeout );
		timeout = setTimeout( later, wait );
		if ( callNow ) func.apply( context, args );
	};
}
  
//  Throttled Scroll Watcher
//    window.addEventListener("optimizedScroll", function() {
//      // Run event code or call function
//    });
( function() {
	var throttle = function ( type, name, obj ) {
		obj = obj || window;
		var running = false;
		var func = function () {
			if ( running ) {
				return;
			}
			running = true;
				requestAnimationFrame( function () {
				obj.dispatchEvent( new CustomEvent( name ) );
				running = false;
			} );
		};
		obj.addEventListener( type, func );
	};
	/* init - you can init any event */
	throttle( "scroll", "optimizedScroll" );
})();


// Toggle Class
const toggleClass = (el, className) => el.classList.toggle(className);
// Toggle Menu Open
const toggleMenu = () => toggleClass(document.querySelector('html'), 'is-menu-open');
// Toggle Sub Menu
const toggleSubMenu = (el) => toggleClass(el, 'is-open');
const showSubMenu = (el) => el.classList.add('is-open');
const hideSubMenu = (el) => el.classList.remove('is-open');
function closeAllSubmenus(current) {
	document.querySelectorAll('.menu-item-has-children').forEach(item => {
		if ( current === item ) {
			return;
		}
		item.classList.remove('is-open');
	});
}

// Watch for Menu Actions (nav, subnav, and menu toggle)
function menuWatch() {
	document.querySelectorAll('.menu-toggle').forEach(item => {
		item.addEventListener('click', () => {
			toggleMenu();
		});
	});

	document.querySelectorAll('.menu-item-has-children button').forEach(item => {
		item.addEventListener('click', (event) => {
			closeAllSubmenus(event.target.closest('.menu-item-has-children'));
			toggleSubMenu(event.target.closest('.menu-item-has-children'));
		});
		item.addEventListener('focusin', (event) => {
			closeAllSubmenus(false);
			showSubMenu(event.target.closest('.menu-item-has-children'));
		});
	});
}

// Watch for Modal Actions 
function modalWatch(button, modal, activeClass) {
	var modalEl = document.querySelectorAll(modal);
	// Open when clicking selected toggle
	document.querySelectorAll(button).forEach(item => {
		item.addEventListener('click', () => {
			document.documentElement.classList.add(activeClass);
		})
	})
	// Close when clicking overlay
	modalEl.querySelector('.modal__overlay').addEventListener('click', event => {
		if (event.target === event.currentTarget) {
			document.documentElement.classList.remove(activeClass);
		}
	});
	// Close when clicking close button
	modalEl.querySelector('.modal__close').addEventListener('click', () => {
		document.documentElement.classList.remove(activeClass);
	});
}

// Check Scroll location. Toggles class
function scrollCheck() {
	var scrollPosY = window.pageYOffset | document.body.scrollTop;
	if ( scrollPosY > 10 ) {
		document.documentElement.classList.add('is-scrolled');
	} else {
		document.documentElement.classList.remove('is-scrolled');
	}
}

function checkMenuAnimate(initialWidth, width) {
	if ( initialWidth >= width ) {
		document.documentElement.classList.remove('is-animated');
		document.documentElement.classList.remove('is-menu-open');
	} else {
		setTimeout(function() { 
			document.documentElement.classList.add('is-animated');
		}, 500);
	}
}