// Test if Google Maps API is loaded. Call loadGoogleMap() function once API is loaded.
function checkForMap(elID) {
	if ( document.getElementById(elID) ) {
		var tryLoad = setInterval(function() {
			if ( typeof google != 'undefined' ) {
				clearInterval(tryLoad);
				loadGoogleMap(elID);
			}
		});
	}
}

// Initialize Google Map
function loadGoogleMap(elID) {
	var url = WP_URL.templateUrl,
		overlayImg = url + '/images/overlay.png',
		mapContainer = document.getElementById(elID),
		initialLoc = {
			lat: parseFloat(mapContainer.attr('data-start-lat')),
			lng: parseFloat(mapContainer.attr('data-start-long'))
		},
		initialZoom = parseInt(mapContainer.attr('data-zoom')),
		mapStyle = getMapStyle();

	// Base Map
	var map = new google.maps.Map(
		document.getElementById('map'), {
			zoom: initialZoom,
			center: initialLoc,
			styles: mapStyle,
			minZoom: 13, 
			maxZoom: 20,
			scrollwheel: false,
			streetViewControl: false,
			mapTypeControl: false,
			fullscreenControl: false,
		}
	);

	// Building Overlay
	var imageBounds = {
		north: 47.607744,
		south: 47.606628,
		east: -122.335359,
		west: -122.337032
	};
	
	var buildingOverlay = new google.maps.GroundOverlay(
		overlayImg,
		imageBounds
	);

	buildingOverlay.setMap(map);
}

// JSON map style. Can be customized & exported at https://snazzymaps.com/
function getMapStyle() {
	return [
		{
			"featureType": "administrative",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "administrative",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#444444"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi.sports_complex",
			"elementType": "labels",
			"stylers": [
				{
					"color": "#707479"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 45
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#ffffff"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#666666"
				},
				{
					"weight": "1"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road.highway.controlled_access",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.highway.controlled_access",
			"elementType": "labels.text",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road.highway.controlled_access",
			"elementType": "labels.icon",
			"stylers": [
				{
					"weight": "1"
				},
				{
					"lightness": "0"
				},
				{
					"gamma": "0.33"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit.line",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit.station",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
				{
					"color": "#d0d2d2"
				},
				{
					"visibility": "on"
				}
			]
		}
	]	
}