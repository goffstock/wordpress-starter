// Filter articles via dropdown/checkbox

function articleFilter(formSelector, articleWrapSelector) {
	var form = document.querySelector(formSelector),
		articleWrap = document.querySelector(articleWrapSelector),
		allArticles = articleWrap.querySelectorAll('.filter__article');
	form.addEventListener('change', () => {
		var selected = form.querySelector('.filter__field:checked, .filter__field:selected'),
			filteredArticles = articleWrap.querySelector('.filter__article--' + selected.val());

		if ( '*' === selected.val() ) {
			allArticles.classList.remove('filter-is-hidden');
		} else {
			allArticles.addClass('filter-is-hidden');
			filteredArticles.removeClass('filter-is-hidden');
		}
	});
}