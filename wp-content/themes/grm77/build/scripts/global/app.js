//------------------------------------------------------------------|
// Site Setup. Call functions & initialize plugins
// inside of window.onload function
//------------------------------------------------------------------|
window.onload = function() {
	// Strict Mode On
	"use strict";

	// Add class to body if js supported. Use this for styling elements requiring JS
	document.body.className += ' is-loaded';
  
	// Toggle menu
	menuWatch();
	scrollCheck();

	// Animations
	if ( document.querySelector( '.animate' ) ) {
		imageReveal('.animate', 250);
	}
	if ( document.querySelector( '.animate-fade' ) ) {
		imageFadeIn('.animate-fade', 250);
	}
	
  
	var initialWidth = jQuery(window).width();
	checkMenuAnimate(initialWidth, 1280);
	var scrollDestroyed = false;
	// Re-call some functions on screen re-size
	var screenResizeHandler = debounce( function () {
		if ( initialWidth !== jQuery(window).width() ) {
			initialWidth = jQuery(window).width();
			scrollCheck();
		}
		if ( scrollDestroyed === false ) {
			ScrollReveal().destroy();
			document.documentElement.classList.remove('sr');
			scrollDestroyed = true;
		}
		checkMenuAnimate(initialWidth, 1280);
	}, 125 );

	window.addEventListener( 'resize', screenResizeHandler, true );
	
	var screenScrollHandler = debounce( function () {
		document.documentElement.classList.remove('is-animated');
		scrollCheck();
		document.documentElement.classList.add('is-animated');
	}, 0.05 );
	window.addEventListener( 'scroll', screenScrollHandler );  

};