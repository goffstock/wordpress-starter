/* ------------------------------------------------------------------------------
 * Gulp build for Geoff Mortstock
 *
 * Table of Contents:
 *
 * 	a. Instructions
 *  b. Dependencies
 *  c. Paths
 * 01. Scripts
 * 02. Styles
 * 03. Linting
 * 04. Testing
 * 05. Combined tasks
 * 06. Default task
 * --------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------------
 * a. Instructions
 * ---------------------------------------------------------------------------- 

npm i 			: Install all dependencies
gulp 			: Build
gulp watch 		: Watch for all updates
gulp lint 		: Lint only
gulp scripts 	: Scripts only
gulp styles 	: Styles only

*/

/* ------------------------------------------------------------------------------
 * b. Dependencies
 * ---------------------------------------------------------------------------- */

const gulp = require( 'gulp' );
const sass = require( 'gulp-sass' );
const autoprefixer = require( 'gulp-autoprefixer' );
const rename = require( 'gulp-rename' );
const babel = require('gulp-babel');
const jshint = require( 'gulp-jshint' );
const phpcs = require( 'gulp-phpcs' );
const stylelint = require( 'gulp-stylelint' );
const browserSync = require( 'browser-sync' );
const concat = require( 'gulp-concat' );
const terser = require( 'gulp-terser' );
const notify = require( 'gulp-notify' );

sass.compiler = require( 'node-sass' );

/* ------------------------------------------------------------------------------
 * c. Paths
 * ---------------------------------------------------------------------------- */

const paths = {
	'sass': 'build/sass/**/*.scss',
	'scripts': 'build/scripts/**/*.js',
	'modulesjs': 'build/scripts/modules/**/*.js',
	'php': [ '**/*.php', '!node_modules' ]
};

/* ------------------------------------------------------------------------------
 * 01. Scripts
 * ---------------------------------------------------------------------------- */

/*
Useful optional Sources:
	'node_modules/modernizr/modernizr.js',
		npm install modernizr --save TODO: Update path
	'node_modules/waypoints/lib/jquery.waypoints.js',
	'node_modules/waypoints/lib/shortcuts/sticky.js',
		npm -i waypoints --save
	'node_modules/slick-carousel/slick/slick.js'
		npm i slick-carousel --save
	'node_modules/scrollreveal/dist/scrollreveal.js'
		npm install scrollreveal --save
	'node_modules/foundation-sites/bower_components/fastclick/lib/fastclick.js',


*/

/**
 * Main theme script
 */
gulp.task( 'scripts:global', function() {
	return gulp.src( [
			'build/scripts/global/**/*.js'
		] )
		.pipe(babel({
			presets: ['@babel/preset-env']
		}))
		.pipe( concat( 'global.js' ) )
		.pipe( gulp.dest( 'js/' ) )
		.pipe( rename( {
			suffix: '.min'
		} ) )
		.pipe( terser() )
		.pipe( gulp.dest( 'js/' ) );
} );

/**
 * Build admin.js from individual files
 */
gulp.task( 'scripts:admin', function() {
	return gulp.src( [
			'build/scripts/admin/**/*.js'
		] )
		.pipe(babel({
			presets: ['@babel/preset-env']
		}))
		.pipe( concat( 'admin.js' ) )
		.pipe( gulp.dest( 'js/' ) )
		.pipe( rename( {
			suffix: '.min'
		} ) )
		.pipe( terser() )
		.pipe( gulp.dest( 'js/' ) );
} );

/**
 * Slick carousel script
 * we provide the uncompressed version for QA but enqueue the compressed version
 *
 * https://kenwheeler.github.io/slick/
 */
gulp.task( 'scripts:slick', function() {
	return gulp.src( [
			'node_modules/slick-carousel/slick/slick.js',
			'node_modules/slick-carousel/slick/slick.min.js'
		] )
		.pipe( gulp.dest( 'js/' ) );
} );

/**
 * Scroll Reveal Script
 * we provide the uncompressed version for QA but enqueue the compressed version
 *
 * TODO: UPDATE PATH
 * 
 * https://scrollrevealjs.org/
 */
gulp.task( 'scripts:scrollreveal', function() {
	return gulp.src( [
			'node_modules/scrollreveal/dist/scrollreveal.js',
			'node_modules/scrollreveal/dist/scrollreveal.min.js'
		] )
		.pipe( gulp.dest( 'js/' ) );
} );

/**
 * Module scripts
 * Each file generates an individual compiled file
 */
gulp.task( 'scripts:modules', function() {
	return gulp.src( [
			paths.modulesjs
		] )
		.pipe( gulp.dest( 'js/' ) )
		.pipe( rename( {
			suffix: '.min'
		} ) )
		.pipe( terser() )
		.pipe( gulp.dest( 'js/' ) );
} );

/* ------------------------------------------------------------------------------
 * 02. Styles
 * ---------------------------------------------------------------------------- */

/**
 * Style sheet creation for extra style sheets (non-theme styles)
 */
gulp.task( 'styles:extra', function() {
	return gulp.src( [
			'build/sass/**/*.scss', '!build/sass/style.scss'
		] )
		.pipe( sass( {
				outputStyle: 'expanded'
			} )
			.on( 'error', notify.onError( function( error ) {
				return 'Sass error in styles:extra : ' + error.message;
			} ) )
		)
		.pipe( autoprefixer() )
		.pipe( gulp.dest( 'css/' ) )
		.pipe( rename( {
			suffix: '.min'
		} ) )
		.pipe( sass( {
				outputStyle: 'compressed'
			} )
			.on( 'error', notify.onError( function( error ) {
				return 'Sass error in styles:extra compressed : ' + error.message;
			} ) )
		)
		.pipe( gulp.dest( 'css/' ) );
} );

/**
 * Stylesheet for theme
 * Create an uncompressed version in /css for QA purposes
 * Create compressed version at theme root as styles.css
 */
gulp.task( 'styles:theme', function() {
	return gulp.src( [
			'build/sass/style.scss'
		] )
		.pipe( sass( {
				outputStyle: 'expanded'
			} )
			.on( 'error', notify.onError( function( error ) {
				return 'Sass error in styles:theme : ' + error.message;
			} ) )
		)
		.pipe( autoprefixer() )
		.pipe( gulp.dest( 'css/' ) )
		.pipe( sass( {
				outputStyle: 'compressed'
			} )
			.on( 'error', notify.onError( function( error ) {
				return 'Sass error in styles:theme compressed : ' + error.message;
			} ) )
		)
		.pipe( gulp.dest( '.' ) );
} );

/* ------------------------------------------------------------------------------
 *  03. Linting
 * ---------------------------------------------------------------------------- */

/**
 * Lint JavaScript
 * @see http://jshint.com/docs/
 */
gulp.task( 'lint:js', function() {
	return gulp.src( [
			'build/scripts/**/*.js'
		] )
		.pipe( jshint() )
		.pipe( jshint.reporter( 'jshint-stylish' ) );
} );

/**
 * Lint PHP for coding style
 * Uses WordPress VIP coding standard
 * @see https://github.com/squizlabs/PHP_CodeSniffer
 * @see https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards
 */
gulp.task( 'lint:php', function() {
	return gulp.src( paths.php )
		.pipe( phpcs( {
			bin: '/usr/local/bin/phpcs',
			standard: 'WORDPRESS-VIP',
			warningSeverity: 0
		} ) )
		.pipe( phpcs.reporter( 'log' ) );
} );

/**
 * Lint sass scss files
 * @see https://stylelint.io
 * @link https://github.com/olegskl/gulp-stylelint
 */
gulp.task( 'lint:sass', function lintCssTask() {

	return gulp
		.src( paths.sass )
		.pipe( stylelint( {
			fix: true,
			reporters: [ {
				formatter: 'string',
				console: true
			} ]
		} ) );
} );

/* ------------------------------------------------------------------------------
 *  04. Testing
 * ---------------------------------------------------------------------------- */

/**
 * Browser Sync
 * @see https://www.browsersync.io
 */
gulp.task( 'browser-sync', function() {
	browserSync.create();
	browserSync.init( {
		proxy: 'vagrant.local',
		notify: false,
		open: false,
		watchOptions: {
			'debounceDelay': 1000
		}
	} );
} );

/* ------------------------------------------------------------------------------
 *  05. Combined tasks
 * ---------------------------------------------------------------------------- */

gulp.task( 'styles', gulp.parallel( 'styles:theme', 'styles:extra' ) );

gulp.task( 'scripts', gulp.parallel( 'scripts:global', 'scripts:admin', 'scripts:slick', 'scripts:scrollreveal', 'scripts:modules' ) );

gulp.task( 'lint', gulp.parallel( 'lint:sass', 'lint:js' ) );

gulp.task( 'watch', function() {
	gulp.watch( paths.sass, gulp.parallel( 'styles:theme', 'styles:extra' ) );
	gulp.watch( paths.scripts, gulp.parallel( 'scripts' ) );
} );

/* ------------------------------------------------------------------------------
 * 06. Default task
 * ---------------------------------------------------------------------------- */

gulp.task( 'default', gulp.series( 'styles', 'scripts', 'lint' ), function( done ) {
	done();
} );
