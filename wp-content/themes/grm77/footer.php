<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GRM77_Theme
 */

?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer__cols">
			<div class="footer__col"></div>
			<div class="footer__col"></div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
function initMap() {
	if ( !document.getElementById('map') ) {
		return;
	}
	var testLocalLoad = setInterval(function() {
		if ( typeof loadGoogleMap === 'function' ) {
			clearInterval(testLocalLoad);
			loadGoogleMap();
		}
	}, 250);
}
</script>
</body>
</html>
