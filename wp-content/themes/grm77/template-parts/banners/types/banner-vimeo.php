<?php 
$video = get_field('banner_video_link');
?>
<div class="banner__video banner__media-wrap">
	<?php echo wp_kses_post($video); ?>
</div>
