<?php 
$video = get_field('banner_video');
?>
<div class="banner__video banner__media-wrap">
	<video id="video" autoplay muted loop>
		<source src="<?php echo esc_url($video['url']); ?>" type="video/mp4">
		<?php esc_html_e('Your browser does not support the video tag.'); ?>
	</video>
</div>
