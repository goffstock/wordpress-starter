<?php
$title = get_the_title();

if ( is_tag() || is_category() ) {
	$title = get_the_archive_title();
}

?>

<div id="banner" class="banner banner--secondary">
	<div class="constrained">
		<div class="banner__title">
			<h1><?php esc_html_e($title); ?></h1>
		</div>
	</div>
</div><!-- .banner -->