<?php
$banner_type = ( is_front_page() ) ? 'static' : get_field('banner_type'); // video, static, slideshow
$banner_class = ( is_front_page() ) ? 'banner--home' : '';

$title = get_bloginfo( 'name' ) . ' ' . get_bloginfo( 'description' );
$logo = get_field('logo_main', 'options');

$static_image = get_post_thumbnail_id();
$slides = get_field('banner_slideshow_images');

?>

<header id="banner" class="banner <?php echo esc_attr($banner_class); ?>">
	<div class="banner__content">
		<img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr($title); ?>" />
	</div>
	<?php
	if ( 'video' === $banner_type ) {
		get_template_part('template-parts/banners/banner', 'video');
	} else if ( 'vimeo' === $banner_type ) {
		get_template_part('template-parts/banners/banner', 'slides');
	} else {
		get_template_part('template-parts/banners/banner', 'static');
	}
	?>
</header><!-- .banner -->
