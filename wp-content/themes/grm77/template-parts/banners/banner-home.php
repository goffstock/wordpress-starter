<?php
$banner_type = get_field('banner_type'); // video, static, slideshow
$logo = get_field('banner_logo');
?>

<div id="banner" class="banner banner--home banner--<?php echo esc_attr($banner_type); ?>">
	<div class="banner__content">
		<img src="<?php echo esc_url($logo); ?>" alt="<?php echo get_bloginfo('name'); ?>" />
	</div>
	<?php get_template_part('template-parts/banners/types/banner', $banner_type); ?>
</div><!-- .banner -->
