<?php
$banner_type = get_field('banner_type'); // video, static, slideshow
?>

<div id="banner" class="banner banner--<?php echo esc_attr($banner_type); ?>">
	<div class="banner__content">
		<?php if ( !get_field('banner_title') || get_field('banner_title') === '' ) : ?>
			<h1 class="banner__title"><?php the_title(); ?></h1>
		<?php else: ?>
			<h1 class="is-hidden"><?php the_title(); ?>: </h1>
			<span class="banner__title"><?php echo wp_kses_post(get_field('banner_title')); ?></span>
		<?php endif; ?>
	</div>
	<?php get_template_part('template-parts/banners/types/banner', $banner_type); ?>
</div><!-- .banner -->
