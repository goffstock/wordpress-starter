<?php
$shortcode = get_sub_field('shortcode');
?>
<div class="row flexForm__wrap">
	<div class="flexForm">
		<?php echo do_shortcode($shortcode); ?>
	</div>
</div>