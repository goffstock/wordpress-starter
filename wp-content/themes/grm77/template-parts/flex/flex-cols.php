<?php
$slug = get_sub_field('slug');
$type = get_sub_field('study_type');
$title = get_sub_field('title');
$text = get_sub_field('text');
$link = get_sub_field('link');
$block_class  = get_sub_field('theme') ? ' cols--' . get_sub_field('theme') : 'cols--white';
$block_class .= ' cols--' . get_sub_field('orientation');
$block_class .= ( get_sub_field('small_title') ) ? ' cols--small' : '';
?>

<div class="row">
	<section class="cols <?php echo esc_attr($block_class); ?>">
		<div class="cols__col cols__content">
			<div class="cols__inner">
				<header>
					<p>
						<?php echo esc_html__($slug, 'site-functionality'); ?>
						<?php if ( $type ) : ?>
							- <span><?php echo esc_html__($type, 'site-functionality'); ?></span>
						<?php endif; ?>
					</p>
					<h2><?php echo esc_html__($title, 'site-functionality'); ?></h2>
				</header>
				<div class="cols__text">
					<?php echo wp_kses_post($text); ?>
				</div>
				<?php if ( $link ) : ?>
					<div class="cols__cta">
						<a class="btn btn--cta" href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link['target']); ?>">
							<span><?php echo esc_html__($link['title']); ?></span>
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="cols__col cols__img">
			<?php 
			if( have_rows('media') ):
				while ( have_rows('media') ) : the_row();
					$image = get_sub_field('image');
					$video = get_sub_field('video');
			?>
					<?php if ( $video ) : ?>
						<video id="video" controls>
							<source src="<?php echo esc_url($video); ?>" type="video/mp4">
							<?php esc_html_e('Your browser does not support the video tag.'); ?>
						</video>
					<?php else : ?>
						<img <?php acf_responsive_image($image['id'],'img-med-3','800px'); ?> />
					<?php endif; ?>
			<?php
				endwhile;
			endif;
			?>
		</div>
	</section>
</div>