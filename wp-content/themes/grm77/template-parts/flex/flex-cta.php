<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
$image = get_sub_field('background_image')['url'];
$link = get_sub_field('link');
?>

<div class="row cta__wrap">
	<section class="cta">
		<div class="cta__content">
			<header>
				<h2><?php echo esc_html__($title); ?></h2>
			</header>
			<div class="cta__text">
				<?php echo wp_kses_post($text); ?>
			</div>
			<?php if ( $link ) : ?>
				<div class="cta__cta">
					<a class="btn btn--cta" href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link['target']); ?>">
						<span><?php echo esc_html__($link['title']); ?></span>
					</a>
				</div>
			<?php endif; ?>
		</div>
		<div class="cta__img-wrap">
			<div class="cta__img">
				<img src="<?php echo esc_url($image); ?>" alt="">
			</div>
		</div>
	</section>
</div>