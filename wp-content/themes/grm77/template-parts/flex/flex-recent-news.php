<?php 
$title = get_sub_field('title');
$articles = get_sub_field('articles');
$category = get_sub_field('category');
?>
<div class="row recent__wrap">
	<section class="recent constrained">
		<header>
			<h2><?php echo esc_html__($title); ?></h2>
		</header>
		<div id="iso" class="recent__grid news__grid">
			<?php 
			if ( $articles ) : 
				foreach( $articles as $post ): 
					setup_postdata($post); 
					get_template_part('template-parts/modules/module', 'news');
				endforeach;
			else :
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 4,
					'orderby'   => 'date',
					'order'     => 'DESC',
				);
				if ( $category ) {
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'category',
							'field'    => 'ID',
							'terms'    => $category,
						),
					);
				}
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) :
					while ( $query->have_posts() ) : $query->the_post();
						get_template_part('template-parts/modules/module', 'news');
					endwhile;
				endif;
			endif;
			wp_reset_postdata();
			?>
		</div>
	</section>
</div>