<?php 
$icon = get_sub_field('icon');
$title = get_sub_field('title');
$text = get_sub_field('text');
$image = get_sub_field('background_image');
$video = get_sub_field('background_video');
$theme = get_sub_field('theme');
?>
<div class="row">
	<div class="media media--<?php echo esc_attr($theme); ?>">
		<div class="media__content">
			<img src="<?php echo esc_url($icon); ?>" alt="" />
			<h2><?php echo esc_html__($title); ?></h2>
			<div class="media__text">
				<?php echo wp_kses_post($text); ?>
			</div>
		</div>
		<div class="media__bg">
			<?php if ( $video ) : ?>
				<video id="video" autoplay muted loop controls>
					<source src="<?php echo esc_url($video); ?>" type="video/mp4">
					<?php esc_html_e('Your browser does not support the video tag.'); ?>
				</video>
			<?php else : ?>
				<img <?php acf_responsive_image($image['id'],'img-max','2200px'); ?> />
			<?php endif; ?>
		</div>
	</div>
</div>