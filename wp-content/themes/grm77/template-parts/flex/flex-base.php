<?php
if( have_rows('page_rows') ):
  while ( have_rows('page_rows') ) : the_row();
    $row_type = get_row_layout();
    get_template_part( 'template-parts/flex/flex', $row_type );
  endwhile;
endif;
?>