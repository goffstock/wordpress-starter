<div>

	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr(get_field('google_maps_api_key', 'options')); ?>&callback=initMap" async defer></script>

</div>