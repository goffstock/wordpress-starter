<?php if ( !empty( get_the_content() ) ) ?>
	<div class="row">
		<div class="wysiwyg">
			<?php the_content(); ?>
		</div>
	</div>
<?php endif; ?>