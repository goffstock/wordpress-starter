<?php
$term = 'category';
if ( is_page_template( 'page-templates/leadership.php' ) || is_page_template( 'page-templates/careers.php' ) ) {
	$term = 'location';
} elseif ( is_page_template( 'page-templates/resources.php' ) ) {
	$term = 'resource_category';
}
$terms = get_terms( array(
	'taxonomy'	=> $term,
	'hide_empty' => true,
	'orderby' => 'name',
	'order' => 'ASC',
));
?>
<div class="filter__wrap">
	<?php if ( $terms && count($terms) > 1 ) : ?>
		<p class="filter__label">Filter</p>
		<ul id="filter" class="filter">
			<li>
				<button class="filter__button is-active" data-term="*">
					<span>All</span>
				</button>
			</li>
			<?php foreach($terms as $term) :
				if ( $term->slug === 'uncategorized' ) { continue; }
			?>
				<li>
					<button class="filter__button" data-term="<?php echo esc_attr($term->slug); ?>">
						<span><?php echo esc_html__($term->name); ?></span>
					</button>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>