<nav id="site-navigation-scrolled" class="main-navigation--scrolled" aria-hidden="true">
	<div class="nav-wrap--scrolled">
		<div class="site-branding">
			<?php 
			$logo_main = ( is_front_page() && is_home() ) ? get_field('light_logo', 'options') : get_field('light_dark', 'options');
			?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home" itemprop="url">
				<img src="<?php echo $logo_main; ?>" class="custom-logo custom-logo--main" alt="<?php echo get_bloginfo('name'); ?>" itemprop="logo">
			</a>
			<?php //the_custom_logo(); ?>
			<?php if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<button type="button" class="menu-toggle" aria-label="toggle menu" aria-controls="primary-menu" aria-expanded="false">
			<span></span>
			<span></span>
			<span></span>
		</button>

		<div class="menu-wrap">		
		<?php
			wp_nav_menu( array(
				'theme_location' => 'primary',
				'menu_id'        => 'primary-menu-scrolled',
				'menu_class'     => 'nav-menu',
				'container'		 => false,
				'walker' 		 => new WEC_Sub_Menu_Icon_Walker(),
			) );
			?>
		</div>
	</div>
</nav>