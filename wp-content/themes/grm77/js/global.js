"use strict";

//------------------------------------------------------------------|
// Site Setup. Call functions & initialize plugins
// inside of window.onload function
//------------------------------------------------------------------|
window.onload = function () {
  // Strict Mode On
  "use strict"; //------------------------------------------------------------------|
  // Cut the Mustard Check
  // Check if browser supports modern js. Don't load wrapped JS if
  // it doesn't pass this check.
  // Ensure base non-js functionality when JS doesn't load.
  //------------------------------------------------------------------|

  var supports = !!document.querySelector && !!window.addEventListener;

  if (!supports) {
    return;
  } // Add class to body if js supported. Use this for styling elements requiring JS


  document.body.className += ' has-modern-js is-loaded';

  if (document.querySelector('.home ')) {
    navWatch();
  }

  if (document.getElementById('dynamic__map')) {
    var tryLoad = setInterval(function () {
      if (typeof google != 'undefined') {
        clearInterval(tryLoad);
        loadGoogleMap();
      }
    });
  }

  if (document.getElementById('brochure-download')) {
    loadBrochure();
  } // Toggle menu


  if (document.getElementById('menu-toggle')) {
    document.getElementById("menu-toggle").addEventListener('click', toggleMenu, false);
  } // Animations


  if (document.querySelector('.animate')) {
    imageReveal('.animate', 250);
  }

  if (document.querySelector('.animate-fade')) {
    imageFadeIn('.animate-fade', 250);
  } // Load towers


  if (document.querySelector('.towers')) {
    towerSetup();
  } // Load carousel slideshow


  if (document.querySelector('.carousel')) {
    startCarousel();
  } // Newsletter dropdown


  if (document.querySelector('#news-select')) {
    newsSelector();
  }

  scrollCheck();
  var initialWidth = jQuery(window).width(); // Re-call some functions on screen re-size

  var screenResizeHandler = debounce(function () {
    if (initialWidth !== jQuery(window).width()) {
      initialWidth = jQuery(window).width();
      scrollCheck();
    }

    if (document.documentElement.classList.contains('is-menu-open')) {
      resizeCloseMenu();
    }
  }, 125);
  window.addEventListener('resize', screenResizeHandler, true);

  window.onbeforeunload = function () {
    window.scrollTo(0, 0);
  };
};
"use strict";

//----------------------------------------------|
// Functions
//----------------------------------------------|
// Debounce
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
} //  Throttled Scroll Watcher
//    window.addEventListener("optimizedScroll", function() {
//      // Run event code or call function
//    });


(function () {
  var throttle = function throttle(type, name, obj) {
    obj = obj || window;
    var running = false;

    var func = function func() {
      if (running) {
        return;
      }

      running = true;
      requestAnimationFrame(function () {
        obj.dispatchEvent(new CustomEvent(name));
        running = false;
      });
    };

    obj.addEventListener(type, func);
  };
  /* init - you can init any event */


  throttle("scroll", "optimizedScroll");
})(); //  Open/close Menu


function toggleMenu() {
  if (document.documentElement.classList.contains('is-menu-open')) {
    document.documentElement.classList.remove('is-menu-open');
  } else {
    document.documentElement.classList.add('is-animated');
    document.documentElement.classList.add('is-menu-open');
  }
}

function resizeCloseMenu() {
  document.documentElement.classList.remove('is-animated');
  document.documentElement.classList.remove('is-menu-open');
} // Check Scroll location


function scrollCheck() {
  var scrollPosY = window.pageYOffset | document.body.scrollTop;

  if (scrollPosY > 10) {
    document.body.classList.add('is-scrolled');
  } else if (scrollPosY <= 10) {
    document.body.classList.remove('is-scrolled');
  }
}