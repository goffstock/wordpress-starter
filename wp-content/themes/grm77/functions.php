<?php
/**
 * Starter 77 Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GRM77_Theme
 */

if ( ! function_exists( 'grm77_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function grm77_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Starter 77 Theme, use a find and replace
		 * to change 'grm77' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'grm77', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'grm77' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'grm77_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		// Remove unused features
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	
	}
endif;
add_action( 'after_setup_theme', 'grm77_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function grm77_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'grm77_content_width', 640 );
}
add_action( 'after_setup_theme', 'grm77_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function grm77_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'grm77' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'grm77' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'grm77_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function grm77_scripts() {
	$theme = wp_get_theme();
	$version =  get_bloginfo( 'version' ) . '-' . $theme->get( 'Version' );

	wp_enqueue_style( 'grm77-style', get_stylesheet_uri(), array(), $version );
	//wp_enqueue_style( 'grm77-fonts', 'https://use.typekit.net/qkh8lpm.css', false ); 
	wp_enqueue_script( 'grm77-navigation', get_template_directory_uri() . '/js/navigation.min.js', array(), '20151215', true );
	wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js' , array() , '2.2.2', false);
	wp_enqueue_script( 'grm77-slick', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'grm77-scrollreveal', get_template_directory_uri() . '/js/scrollreveal.min.js', array( ), false, true );
	wp_enqueue_script( 'grm77-script', get_template_directory_uri() . '/js/global.min.js', array( 'jquery', 'grm77-scrollreveal', 'grm77-slick' ), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	//https://stackoverflow.com/questions/5221630/wordpress-path-url-in-js-script-file
	$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
	wp_localize_script( 'grm77-script', 'WP_URL', $translation_array );
}
add_action( 'wp_enqueue_scripts', 'grm77_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/walker.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add template functionality
 */
require get_template_directory() . '/inc/site-options.php';
require get_template_directory() . '/inc/image-functions.php';
require get_template_directory() . '/inc/api.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

