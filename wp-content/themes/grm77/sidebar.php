<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GRM77_Theme
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
wp_enqueue_style( 'grm77-sidebar-style', get_template_directory_uri() . '/css/sidebar.min.css' );
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
